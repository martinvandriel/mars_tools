# Mars Tools

This package includes

 - A hack to adapt the ObsPy FDSN client to the non-standard configuration of MSDS.

 - An approximate conversion between LMST at the insight landing site and UTC.

 - A tool to calculate spectrograms for a quick check of data chunks of a few hours to days


## Installation with pip

clone or download to disk, then

```
cd mars_tools
pip install -v -e .
```

## FDSN Client Usage

Inherits all features from the obspy fdsn client, just changes the authentication to basic auth and the route used for authenticated queries to `/query`. Refer to the obspy documentation for any question other than authentication:

https://docs.obspy.org/packages/autogen/obspy.clients.fdsn.client.Client.html

### Minimal example

```
from marstools import Client
from obspy import UTCDateTime

starttime = UTCDateTime('2018-12-01T00:00:00')                              
endtime = UTCDateTime('2019-12-31T00:00:00')                                

client = Client('https://ws.seis-insight.eu', user=user, password=password)

inventory = client.get_stations(network="XB", station="ELYS0", channel='MH?', 
                                starttime=starttime, endtime=endtime,
                                level='channel')           
 
print(inventory)
```

## Time converter usage

```
from mars_tools import insight_time
insight_time.UTCify(0.)
2018-11-27T05:48:53.944147Z
```

Sols are treated as time since January 1st 1970, similar to Earth times:

```
insight_time.solify(UTCDateTime('2018-11-27T05:48:53.944147'))                                 
1970-01-01T00:00:00.000000Z
```

Current Sol:

```
insight_time.solify(UTCDateTime.now()).day
```


## Spectrogram usage
This code creates spectrograms of one or multiple miniseed files split to a long 
period window below 1 Hz and a high frequency window above 1 Hz. Additionally,
it calculates median PSDs over the time period.

It can handle multiple input files with gaps between.
```
python -m mars_tools.spectrogram -lp PATH_TO_LONG_PERIOD_FILES \
                                 -hf PATH_TO_HIGH_FREQUENCY_FILES
```


