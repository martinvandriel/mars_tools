#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
An approximate time converter for UTC and LMST at the insight landing site.

:copyright:
    Simon Stähler (mail@simonstaehler.com), 2018
    Martin van Driel (Martin@vanDriel.de), 2018
:license:
    None
'''
from obspy import UTCDateTime

SEC_PER_DAY_EARTH = 86400
SEC_PER_DAY_MARS = 88775.2440

def solify(UTC_time, sol0=UTCDateTime(2018, 11, 26, 5, 10, 50.33508)):

    MIT = (UTC_time - sol0) / SEC_PER_DAY_MARS
    t = UTCDateTime((MIT - 1) * SEC_PER_DAY_EARTH)
    return t


def UTCify(LMST_time, sol0=UTCDateTime(2018, 11, 26, 5, 10, 50.33508)):

    MIT = float(LMST_time) / SEC_PER_DAY_EARTH + 1
    UTC_time = UTCDateTime(MIT * SEC_PER_DAY_MARS + float(sol0))
    return(UTC_time)

def convert_lmst_string(string):
    # Convert the output string of CHRONOS to a UTCDateTime object
    time_string = '1970-01-01T' + string[6:] + '0Z'
    lmst = UTCDateTime(time_string) \
           + 86400 * (float(string[0:5]) - 1.)
    return lmst


if __name__ == "__main__":
    # test against CHRONOS time INSIGHT_OPS181206_V
    times = [
    ['2023-01-01', '01456M17:40:44.72779'],
    ['2022-01-01', '01101M12:03:32.55550'],
    ['2021-01-01', '00746M06:26:20.38322'],
    ['2020-01-01', '00390M01:27:39.90359'],
    ['2019-01-01', '00034M19:50:27.73129']]
    import matplotlib.pyplot as plt
    for time in times:
        lmst_chronos = convert_lmst_string(time[1])
        utc_in = UTCDateTime(time[0])
        print(utc_in, lmst_chronos, solify(utc_in))
        plt.plot(utc_in.datetime, lmst_chronos - solify(utc_in), 'o')
    plt.show()

    # utc_in = [UTCDateTime(2018, 11, 26, 5, 10, 49.9),
    #           UTCDateTime('2018-12-13T00:00'),
    #           UTCDateTime('2022-12-13T00:00')]

    # lmst_chronos_in = [UTCDateTime('1970-01-01') - 86400.,
    #                    UTCDateTime('1970-016Z08:02:29.89167'),
    #                    UTCDateTime('19700101T05:52:46.88817') + 86400 * 1437.]
    # for utc, lmst_chronos in zip(utc_in, lmst_chronos_in):
    #     lmst = solify(utc)
    #     print(utc)
    #     print(lmst)
    #     print(lmst_chronos)

    #     utc = UTCify(lmst)
    #     print(utc)
    #     print()
