#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Small python script to download data from MSDS.

:copyright:
    Simon Staehler (simon.staehler@erdw.ethz.ch), 2018
:license:
    None
'''
from os.path import join as pjoin
from os import makedirs
from obspy import UTCDateTime

from mars_tools.msds_fdsn_client import Client
import argparse

def def_args():
    helptext = 'Download InSight data from MSDS and remove the instrument' + \
               'response. MSDS username and password are required arguments.'

    parser = argparse.ArgumentParser(helptext)

    parser.add_argument('username', help='MSDS username')
    parser.add_argument('password', help='MSDS password')
    parser.add_argument('--tstart', default='2018-11-27',
                        help='Start time for spectrogram' +
                             '(in any format that datetime understands)')
    parser.add_argument('--tend', default='2020-12-31',
                        help='End time for spectrogram. Default: 2020/12/31')

    parser.add_argument('--stat', default='ELYS0',
                        help='Station code(s) to download')
    parser.add_argument('--chan', default='MH?',
                        help='Channel code(s) to download')
    parser.add_argument('--loc', default='??',
                        help='Location code(s) to download')

    parser.add_argument('--no_corr', default=False, action='store_true',
                        help='Omit removing the instrument response')

    parser.add_argument('--gain', default=False, action='store_true',
                        help='Multiply gain only')

    parser.add_argument('--rotate', default=False, action='store_true',
                        help='Rotate to ZNE')

    return parser.parse_args()


if __name__ == '__main__':
    args = def_args()

    dir_raw = 'output_raw'
    dir_rot = 'output_rot'
    dir_corr = 'output_corr'
    dir_corr_rot= 'output_corr_rot'
    fnam_string = "{network}.{location}.{station}.{channel}" + \
                  "-{starttime.year}.{starttime.julday:03d}" + \
                  "T{starttime.hour:02d}{starttime.minute:02d}" + \
                  "-{endtime.year}.{endtime.julday:03d}" + \
                  "T{endtime.hour:02d}{endtime.minute:02d}"


    client = Client('https://ws.seis-insight.eu',
                    user=args.username,
                    password=args.password)

    # Data from 1st cruise checkout
    starttime = UTCDateTime(args.tstart)
    endtime = UTCDateTime(args.tend)

    print('Downloading inventory...')
    inv = client.get_stations(network="XB",
                              station=args.stat,
                              location=args.loc,
                              channel=args.chan,
                              starttime=starttime,
                              endtime=endtime,
                              level='response')
    fnam = 'inventory.xml'
    print(inv)
    inv.write(fnam, format='STATIONXML')

    print('Downloading data...')
    st = client.get_waveforms(network="XB",
                              station=args.stat,
                              location=args.loc,
                              channel=args.chan,
                              starttime=starttime,
                              endtime=endtime)

    st_gain = st.copy()
    if args.gain:
        for tr in st_gain:
            response = inv.get_response(tr.get_id(),
                                        tr.stats.starttime + 10)
            gain = response._get_overall_sensitivity_and_gain()[1]
            tr.data = tr.data / gain

    print('Writing uncorrected data to output_raw...')
    makedirs(dir_raw, exist_ok=True)
    for tr in st_gain:
        fnam = pjoin(dir_raw, fnam_string.format(**tr.stats))
        tr.write(fnam, format='mseed')

    if args.rotate:
        print('Rotating to ZNE')
        st_rot = st_gain.copy()

        for tr in st_rot:
            tr.stats.starttime += 10.
        st_rot._rotate_to_zne(inv, components=('UVW'))
        for tr in st_rot:
            tr.stats.starttime -= 10.

        makedirs(dir_rot, exist_ok=True)
        for tr in st_rot:
            fnam = pjoin(dir_rot, fnam_string.format(**tr.stats))
            tr.write(fnam, format='mseed')

    if not args.no_corr:
        print('Removing instrument response from seismic channels...')
        makedirs(dir_corr, exist_ok=True)
        st.detrend(type='demean')
        st.taper(0.1)
        st.detrend()
        st_seis = st.select(channel='?[LH]?')
        for tr in st_seis:
            try:
                tr.remove_response(inv, pre_filt=(0.001, 0.002, 50, 60),
                                   output='VEL')
            except ValueError:
                print('Could not remove response for channel:')
                print(tr)
            else:
                fnam = pjoin(dir_corr, fnam_string.format(**tr.stats))
                tr.write(fnam, format='mseed')

        if args.rotate:
            print('Rotating to ZNE')
            st_rot = st_seis.copy()
            print(st_rot)
            st_rot.merge(method=1)
            print(st_rot)
            st_rot._rotate_to_zne(inv, components=('UVW'))
            makedirs(dir_corr_rot, exist_ok=True)
            for tr in st_rot:
                fnam = pjoin(dir_corr_rot, fnam_string.format(**tr.stats))
                tr.write(fnam, format='mseed')
