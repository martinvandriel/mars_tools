#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
A hack to adapt the obspy client to the non-standard msds fdsn server

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2018
:license:
    None
'''
from .msds_fdsn_client import Client
