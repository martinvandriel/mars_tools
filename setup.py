#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Some python tools for the InSight mars mission.

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2018
    Simon Stähler (mail@simonstaehler.com)
'''
from setuptools import setup, find_packages

setup(name='mars_tools',
      version='0.1',
      description='Some python tools for the InSight mars mission.',
      url='https://gitlab.com/martinvandriel/mars-tools',
      author='Martin van Driel, Simon Stähler',
      author_email='vandriel@erdw.ethz.ch',
      license='None',
      packages=find_packages(), install_requires=['obspy'])
